# simslab-color

# Environment
* python 3.7
* Docker 20.10.12
* Django 3.2.13
* PostgreSQL 12.9
* Ubuntu 20.04

# Requirements
* psycopg2-binary
